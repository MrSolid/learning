export class Like {
  private _isLiked: boolean = false;

  constructor(
    private _likesAmount: number) {
  }

  click() {
    this._likesAmount = this._isLiked 
      ? this._likesAmount - 1 
      : this._likesAmount + 1;
    
    this._isLiked = !this._isLiked;
  }

  get likesAmount() {
    return this._likesAmount;
  }

  get isLiked() {
    return this._isLiked;
  }
}
